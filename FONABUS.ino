
#include <Adafruit_SleepyDog.h>
#include <SoftwareSerial.h>
#include "Adafruit_FONA.h"
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_FONA.h"
#include <SoftwareSerial.h>

#define FONA_RX 2
#define FONA_TX 3
#define FONA_RST 4
#define FONA_APN "kolbi3g"
#define FONA_USERNAME  ""
#define FONA_PASSWORD  ""
#define DEVICE_ID "76545-AAA"
#define PIN "3891"
#define PUBLIC_KEY "pw8NY7yRGlC41axyrYoj"
#define PRIVATE_KEY "64916nmzo2hEdAeqPn6g"
#define DEVICE_ID "xaxaxax"
void flushSerial();

SoftwareSerial fonaSS = SoftwareSerial(FONA_TX, FONA_RX);
SoftwareSerial *fonaSerial = &fonaSS;

Adafruit_FONA fona = Adafruit_FONA(FONA_RST);


void setup() {
  
  //INIT & FIND THE FONA MODULE
  Serial.begin(115200);
  Serial.println(F("FONA Initialization..."));
  
  fonaSerial->begin(4800);

  while(!fona.begin(*fonaSerial)) {
    Serial.print(F("Finding FONA...\n"));
    delay(1000);
  }
  Serial.println(F("FONA FOUND"));

  // Print module IMEI number.
  char imei[16] = {0}; // MUST use a 16 character buffer for IMEI!
  uint8_t imeiLen = fona.getIMEI(imei);
  if (imeiLen > 0) {
    Serial.print("Module IMEI: "); Serial.println(imei);
    
  }
  else {
    Serial.print("Could not find the IMEI "); 
  }

  unlockSIM();
  setGPRS ();
  setGPS ();

  //setting the time
  if (fona.enableRTC(1)) {
    fona.enableNetworkTimeSync(true);
    fona.enableNTPTimeSync(true,F(""));
  
  }
  else {
    Serial.print(" ERROR ON:  Network Time: RTC ");
  } 

  

  
}

void loop() {
  delay(2000);

  float latitude, longitude, speed_kph, heading, speed_mph, altitude;

  // if you ask for an altitude reading, getGPS will return false if there isn't a 3D fix
  boolean gps_success = fona.getGPS(&latitude, &longitude, &speed_kph, &heading, &altitude);
  if (true) {

    Serial.print("GPS lat:");
    Serial.println(latitude, 6);
    Serial.print("GPS long:");
    Serial.println(longitude, 6);
    Serial.print("GPS speed KPH:");
    Serial.println(speed_kph);
    Serial.print("GPS speed MPH:");
    speed_mph = speed_kph * 0.621371192;
    Serial.println(speed_mph);
    Serial.print("GPS heading:");
    Serial.println(heading);
    Serial.print("GPS altitude:");
    Serial.println(altitude);

  } else {
    Serial.println("Waiting for FONA GPS 3D fix...");
  }

  char buffer_date[23];
  // read the time
  fona.getTime(buffer_date, 23);  // make sure replybuffer is at least 23 bytes!
  Serial.print("Network Time: ");
  Serial.println(buffer_date);  
  String date(buffer_date);


  
  
  
    
  

    uint16_t statuscode;
    delay(1000);
    String url_str = "http://35.163.68.74/api/devices/865067021600179/locations\n";
    //String url_str = "http://httpbin.org/post\n";
    //String url_str = "https://reqres.in/api/users\n";
    //String url_str = "35.163.68.74:3000/api/devices/865067021600179/locations\n";
    //String json_str = "{\"speed\" : \"02\" ,\"altidude\": \"12\" , \"latitude\" : \"89\" , \"longitude\" : \"20\"}\n";
    String json_str = "{\"latitude\" :";
    json_str += "\"" + String(latitude,9) + "\",";
    json_str += "\"longitude\" :";
    json_str += "\"" + String(longitude,9) + "\",";
    json_str += "\"speed\" :";
    json_str += "\"" + String(speed_mph,9) + "\",";
    json_str += "\"date\" :";
    json_str +=  date + ",";
    json_str += "\"altitude\" :";
    json_str += "\"" + String(altitude,9);
    json_str +=  "\"}\n";

  

    char url[url_str.length()];
    char data[json_str.length()];
  
    int16_t length = url_str.length();
    flushSerial();
  
    url_str.toCharArray(url, url_str.length());
    json_str.toCharArray(data, json_str.length());
  
    Serial.println(url);
    Serial.println(data);

    if (!fona.HTTP_POST_start(url, F("application/json"), (uint8_t *) data, strlen(data), &statuscode, (uint16_t *)&length)) {
          Serial.println("Failed POST!");
    }
    while (length > 0) {
      while (fona.available()) {
        char c = fona.read();
        // Serial.write is too slow, we'll write directly to Serial register!
        #if defined(__AVR_ATmega328P__) || defined(__AVR_ATmega168__)
          loop_until_bit_is_set(UCSR0A, UDRE0); /* Wait until data register empty. */
          UDR0 = c;
        #else
          Serial.write(c);
        #endif
        length--;
        if (! length) break;
      }        
    }
    Serial.println(F("\n****"));
    fona.HTTP_POST_end();
  
  

}


void flushSerial() {
  while (Serial.available())
    Serial.read();
}

void unlockSIM(){
  //UNLOCK THE SIM CARD   
  Serial.print(F("Unlocking SIM card: "));
  while(! fona.unlockSIM(PIN)) {
    Serial.println(F("Trying to unlocking SIM CARD"));
    delay(3000);
    
  }
  Serial.println(F("OK! SIM CARD unlocked")); 
}
void setGPRS () {
  //ENABLE GPRS
   flushSerial();
   Serial.print(F("SETTING APN: ")); Serial.println(F(FONA_APN));
   fona.setGPRSNetworkSettings(F(FONA_APN),F(FONA_USERNAME),F(FONA_PASSWORD));
   Serial.println(F("Disabling GPRS"));
   Watchdog.reset();
   fona.enableGPRS(false);
   delay(5000);  // wait a few seconds to stabilize connection
   Watchdog.reset();
   while(!fona.enableGPRS(true)) {
       Serial.println(F("Trying to turn GPRS on")); 
       delay(2000);       
   }
   Serial.println(F("Enabled GPRS"));
   Watchdog.reset();
}
void setGPS () {
  //ENABLE GPS
   Serial.println(F("Enabling GPS..."));
   while(!fona.enableGPS(true)){
        Serial.println(F("Trying to turn GPS on")); 
   }
   Serial.println(F("Enabled GPS on")); 
   
}